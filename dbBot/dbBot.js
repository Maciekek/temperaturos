const mongoose = require('mongoose');
const moment = require('moment');
const Temp = require('../models/temp.model');
const tempController = require("../piController/temp");
const axios = require('axios');

const URL = "http://109.241.112.177:81/ping";

class DB {
  constructor() {
    mongoose.connect('mongodb://localhost/app');

    this.db = mongoose.connection;
    this.db.on('error', console.error.bind(console, 'connection error:'));
    this.db.once('open', () => {
      console.log("Connection Successful!");
    });

    setInterval(() => {
      const data = {id: Math.random(), timestamp: moment().toDate().getTime(), date: moment(), temps: tempController.getFullTempsData()}
      new Temp(data).save();

      console.log("ADDED");
      axios.post(URL, {
        data: JSON.stringify(data)
      })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    }, 120000);

  }
}

const db = new DB();