const tempController = require("../piController/temp");
const axios = require('axios');

const URL = "http://109.241.112.177:81/actualTemp";

class TempBot {
  constructor() {

    setInterval(() => {
      const data = {temps: tempController.getFullTempsData()};

      axios.post(URL, {
        data: JSON.stringify(data)
      })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    }, 3000);

  }
}

const tempBot = new TempBot();