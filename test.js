const temp = require("./piController/temp");
const logger = require('morgan');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const path = require('path');
const DB = require('./db/db')
const indexRouter = require('./api/index');
const express = require('express');

let app = express();


const port = process.env.PORT || 80;
const router = express.Router();

// view engine setup
app.use(logger('dev'));
app.use(express.json());

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/build')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(cors());

app.get('/', (req, res) => {
  res.send(temp.getTempsWithNames());
});

app.use('/api', indexRouter);

router.get('*', function(req, res, next) {
  res.sendFile('public/build/index.html');
});

app.listen(port, function () {
  console.log("Running on port " + port);
});
