const mongoose = require('mongoose');
const Temp = require('../models/temp.model');

class DB {
  constructor() {
    console.log("hello!");
    mongoose.connect('mongodb://localhost/app');
    this.db = mongoose.connection;

    this.db.on('error', console.error.bind(console, 'connection error:'));
    this.db.once('open', function () {
      console.log("Connection Successful!");
    });

    // var tempSchema = new mongoose.Schema({
    //   sensorId: String,
    //   temp: String,
    //   date: Date,
    // });
    //
    // var Temp = mongoose.model('Temps', tempSchema);
    //
    // var temp1 = new Temp({ sensorId: 1, temp: 21.2, date: new Date() });
    //
    //
    // temp1.save(function (err) {
    //   if (err) return console.error(err);
    //   // fluffy.speak();
    // });
    //
    // Temp.find(function (err, temps) {
    //   if (err) return console.error(err);
    //   console.log(temps);
    // })

  }

  getData() {
    return new Promise((resolve, reject) => {
      Temp.find().limit(1000).sort('date').exec(function(err, temps) {
        resolve(temps);
      });
    });
  }
}
const db = new DB;
module.exports = db;
