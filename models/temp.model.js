const mongoose = require('mongoose');

// var tempSchema = new mongoose.Schema({
//   sensorId: String,
//   temp: String,
//   date: Date,
// });

var tempSchema = new mongoose.Schema({
  date: Date,
  timestamp: Number,
  temps : [
    {
      id: String,
      temp: Number,
      name: String
    }
  ]
});
//
// {
//   id: String,
//     date: Date,
//   temps : [
//
//   { sensorId: String, temp: Number, sensorName: String }
//
// ]
// }

// Export the model
module.exports = mongoose.model('Temps', tempSchema);