import React, { Component } from 'react';
import './App.css';
import HomeComponent from "./pages/home/home";
import { Jumbotron, Container } from 'reactstrap';
import socketIOClient from "socket.io-client";

class App extends Component {
  render() {
    return (
      <Jumbotron>
        <Container>
          <HomeComponent></HomeComponent>
        </Container>
      </Jumbotron>
    );
  }
}

export default App;
