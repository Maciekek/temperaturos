import React, {Component} from 'react';
import {Card, CardText, CardTitle} from 'reactstrap';
import {tempActions} from "../../actions/tempActions";
import {Spinner} from "../../components/spinner";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official';

const moment = require('moment-timezone');

class HomeComponent extends Component {
  constructor() {
    super();

    this.state = {
      tempData: {},
      series: []
    };
  }

  updateChart = (data) => {
    const series = [];

    let sensorsCount = data[0].temps.length;

    for (let i = 0; i < sensorsCount; i++) {
      series.push({data: [], name: data[0].temps[i].name});
    }

    data.map((record) => {
      for (let i = 0; i < sensorsCount; i++) {
        series[i].data.push([moment(record.timestamp).toDate().getTime(), record.temps[i].temp]);
      }
    });


    this.setState({series})
  };

  componentDidMount() {
    tempActions.getActualTemp().then(response => {
      this.setState({tempData: response});
    });

    tempActions.getHistoryTemp().then(temps => {
      this.updateChart(temps);
    });

    setInterval(() => {
      tempActions.getActualTemp().then(response => {
        this.setState({tempData: response});
      });
    }, 10000);
  }

  render() {
    return (
      <div>
        {this.state.tempData.length
          ? this.state.tempData.map(temp => (
            temp ?
              <Card body className="text-center" key={temp.name}>
                <CardTitle>{temp.name}</CardTitle>
                <CardText>{temp.temp} C</CardText>
              </Card>
              : null
          ))
          : <Spinner/>
        }

        <HighchartsReact
          highcharts={Highcharts}
          options={{
            time: {
              timezoneOffset: -moment.parseZone(moment()).utcOffset()
            },
            title: {
              text: 'Temperaturki'
            },
            tooltip: {
              headerFormat: '<div class="chart-tooltip" style="font-size: 19px;">{point.key}<br>',
              footerFormat: '</div>',
              xDateFormat: '%H:%M',
              shared: true

            },
            xAxis: {
              type: 'datetime',
              title: {
                text: ''
              },
              tickWidth: 0,
              lineWidth: 0,
              offset: 4,
              labels: {
                rotation: -45,
                style: {
                  color: '#8D92A4',
                  fontWeight: 'normal',
                  fontSize: '14px'
                }
              },
              gridLineWidth: 0
            },
            series: this.state.series
          }}
        />
      </div>
    )
  }
}

export default HomeComponent;