const getBaseUrl = () => {
  const hostname = window.location.hostname;

  return`http://${hostname}d/api`;
};

const getHostName = () => {
  const hostname = window.location.hostname;

  return`http://${hostname}`;
};

export {getBaseUrl, getHostName}