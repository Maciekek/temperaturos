import {getBaseUrl} from "./utils";

const tempActions = {};

tempActions.getActualTemp = () => {
  console.log("get");
  return fetch(`${getBaseUrl()}/temp`).then(
    (promise) =>  promise.json())
};

tempActions.getHistoryTemp = () => {
  return fetch(`${getBaseUrl()}/history`).then(
    (promise) =>  promise.json())
};


tempActions.post = () => {
  console.log('post');
};

export {tempActions};