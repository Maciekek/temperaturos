///sys/bus/w1/devices/10-000802cbe042/w1_slave
// /Users/maciejkucharski/test/10-000802cbe825

const fs = require('fs');

// const MAIN_DIR = "/Users/maciejkucharski/test";
const MAIN_DIR = "/sys/bus/w1/devices";

const getConfig = () => {
  return fs.readFileSync("./config.json", {encoding: 'utf8'});
};

const config = JSON.parse(getConfig());
let model = [];

const getSensorNames = () => {
  return fs.readdirSync(MAIN_DIR);
};

const getTemp = (sensorName) => {
  const fileContent = fs.readFileSync(`${MAIN_DIR}/${sensorName}/w1_slave`, {encoding: 'utf8'});
  const index = fileContent.indexOf('t=');

  let temp = fileContent.slice(index+2, fileContent.length);
  temp = temp / 1000;

  return temp;
};

const getName = (sensorId) => {
  for (let i = 0; i < config.tempSensors.length; i++) {
    if (config.tempSensors[i].id === sensorId) {
      return sensorName = config.tempSensors[i].name;
    }
  }
};

getTempsWithModel = () => {
  // console.log("model", model);
  return model.map(sensor => {
    // console.log("sensor", sensor.id);
    sensor.temp = getTemp(sensor.id);

    return sensor;
  })
};

const getTempsWithNames = () => {

  if (model.length > 0) {
    return getTempsWithModel();
  }

  model = getFullTempsData();

  return model;
};

const getFullTempsData = () => {
  const availableSensorNames = getSensorNames();

  let newModel =  availableSensorNames.map(sensorId => {
    if (sensorId === 'w1_bus_master1') {
      return;
    }

    return {
      id: sensorId,
      name: getName(sensorId),
      temp: getTemp(sensorId)
    }
  }).filter(data => {
    if (data) {
      return data;
    }
  });

  console.log("---")
  console.log(newModel)

  return newModel;
};

console.log(getTempsWithNames());

module.exports = {
  getTempsWithNames: getTempsWithNames,
  getFullTempsData: getFullTempsData
};

