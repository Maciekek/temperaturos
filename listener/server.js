const logger = require('morgan');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const path = require('path');
const express = require('express');

let app = express();

const port = process.env.PORT || 81;
const router = express.Router();

// view engine setup
app.use(logger('dev'));
app.use(express.json());

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/build')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(cors());

app.get('/', (req, res) => {
  console.log('qwe');
  res.send("test")
});

app.post('/ping', (req, res) => {
  // console.log('qwe');
  // console.log(req.body);
  res.send("ping pong")
});

app.post('/actualTemp', (req, res) => {
  console.log("ACTUAL TEMPS")
  console.log(req.body)
  res.send("OK");
});
// app.use('/api', indexRouter);

router.get('*', function(req, res, next) {
  res.sendFile('public/build/index.html');
});

app.listen(port, function () {
  console.log("Running on port " + port);
});
