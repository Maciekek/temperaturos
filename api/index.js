var express = require('express');
var router = express.Router();
var temp = require("../piController/temp");
const DB = require('../db/db')

/* GET home page. */
router.get('/temp', function(req, res, next) {
  res.json(temp.getTempsWithNames());
});

router.get('/history', function(req, res, next) {
  const temps = DB.getData();
  temps.then((data) => {
    res.json(data);
  });
});

module.exports = router;
